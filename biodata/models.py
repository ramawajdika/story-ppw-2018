from django.db import models


class Activity(models.Model):

    date = models.DateField()
    time = models.TimeField()
    activity = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    category = models.CharField(max_length=100)


    def __str__(self):
        return self.activity

# Create your models here.
