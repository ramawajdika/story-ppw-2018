from django.urls import include
from django.contrib import admin
from django.urls import path, re_path
from .views import *

urlpatterns = [

    re_path(r'^home/', view_home, name='home'),
    re_path(r'^about/', view_about, name='about'),
    re_path(r'^record/', view_record, name='record'),
    re_path(r'^interest/', view_interest, name='interest'),
    re_path(r'song-cover', view_songcover, name='songcover'),
    re_path(r'^register/', view_register, name='register'),
    re_path(r'^schedule/', view_schedule, name='schedule'),
    re_path(r'^hasil/', jadwal_post, name='jadwal_post'),
    re_path(r'^hasil_destructed/', commit_destruction, name='commit_destruction'),
    re_path(r'^', view_home, name='home'),

]
