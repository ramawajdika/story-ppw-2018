from django.shortcuts import render
from .forms import Activity_Form
from .models import Activity
from django.http import HttpResponseRedirect

response = {}
# Create your views here.
def view_home(request):
    return render(request, 'homepage.html')

def view_about(request):
    return render(request, 'aboutme.html')

def view_record(request):
    return render(request, 'trackrecord.html')

def view_interest(request):
    return render(request, 'interest.html')

def view_songcover(request):
    return render(request, 'songcover.html')

def view_register(request):
    return render(request, 'jadwal.html',{'activity_form' : Activity_Form})

def view_schedule(request):
    return render(request, 'formresult.html', {'activity_list' : Activity.objects.all()} )

def commit_destruction(request):
    Activity.objects.all().delete()
    return HttpResponseRedirect('/schedule/')

def jadwal_post(request) :

    form = Activity_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['date'] = request.POST['date'] if request.POST['date'] != "" else "unknown date"
        response['time'] = request.POST['time'] if request.POST['time'] != "" else "unknown time"
        response['activity'] = request.POST['activity'] if request.POST['activity'] != "" else "unknown activity"
        response['place'] = request.POST['place'] if request.POST['place'] != "" else "unknown place"
        response['category'] = request.POST['category'] if request.POST['category'] != "" else "unknown category"
        activities = Activity(date=response['date'], time=response['time'],
                          activity=response['activity'], place=response['place'], category=response['category'])
        activities.save()
        return HttpResponseRedirect('/schedule/')
    else:
        return HttpResponseRedirect('/home/')
