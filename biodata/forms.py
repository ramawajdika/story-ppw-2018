from django import forms
import datetime


class Activity_Form(forms.Form):
    error_messages = {
        'required': 'Wajib diisi!',
        'invalid': 'Input tidak valid!',
    }
    attrs = {
        'class': 'form-control'
    }
    attrs_datetime = {
        'class': 'form-control',
        'type': 'date'
    }

    attrs_time = {
        'class': 'form-control',
        'type': 'time'
    }

    date = forms.DateField(label='date', required=True,
                           widget=forms.DateInput(attrs=attrs_datetime))

    time = forms.TimeField(label='time', required=True,
                           widget=forms.TimeInput(attrs=attrs_time))
    activity = forms.CharField(label='activity', required=True,
                               widget=forms.TextInput(attrs=attrs))
    place = forms.CharField(label='place', required=False,
                            widget=forms.TextInput(attrs=attrs))
    category = forms.CharField(label='category', required=False,
                               widget=forms.TextInput(attrs=attrs))

